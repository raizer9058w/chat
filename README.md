# RaZi Chat

### Описание

Общий чат для общения

### В разработке использовались

##### Front-end

React, Redux, Typescript, React-router, Styled-components, WebSocket

### Установка

-Склонировать репозиторий в свою директорию  
-Перейти в корневую директорию  
-Запустить yarn start / npm run start
