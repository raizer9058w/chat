import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';

import Authorization from './authorization';
import Chat from './chat';

export default () => {
  const isLogged = useSelector(store => store.logged);

  return (
    <>
      <Switch>
        <Route exact path="/authorization">
          <Authorization />
          {isLogged && <Redirect to="/chat" />}
        </Route>
        <Route path="/chat">
          {isLogged ? <Chat /> : <Redirect to="/authorization" />}
        </Route>
        {isLogged ? <Chat /> : <Redirect to="/authorization" />}
      </Switch>
    </>
  );
};
