import React from 'react';
import styled from 'styled-components';

import { LoginForm } from '../../components/LoginForm';

const StyledAuthorization = styled.div`
  width: 100vw;
  height: 100vh;
  position: fixed;
  top: 0;
  left: 0;
  background-color: var(--biscotti);
`;

export default () => {
  return (
    <StyledAuthorization>
      <LoginForm />
    </StyledAuthorization>
  );
};
