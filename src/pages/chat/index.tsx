import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import { useSelector, useDispatch } from 'react-redux';
import { Redirect } from 'react-router-dom';
import useWebSocket from 'react-use-websocket';

import { ChatInput } from '../../components/ChatInput';
import {
  UsernameBlock,
  UsernameBlockProps,
} from '../../components/UsernameBlock';
import { ChatWindow, ChatWindowProps } from '../../components/ChatWindow';
import { ModalMessage } from '../../components/ModalMessage';
import { LoadScreen } from '../../components/LoadScreen';

import { logout } from '../../store/action';

const StyledChat = styled.div`
  width: 100vw;
  height: calc(100vh - 75px);
  position: fixed;
  top: 75px;
  left: 0;
  background-color: var(--biscotti);

  .chat-wrapper {
    width: calc(100% - 50px);
    height: calc(100vh - 125px);
    max-width: 1920px;
    display: grid;
    grid-template-columns: 25% calc(75% - 25px);
    grid-template-rows: 1fr 15%;
    grid-gap: 25px;
    padding: 25px;
    margin: auto;
  }
`;

const socketUrl = 'wss://razi-chat-backend.herokuapp.com/';
const errorModalMessage =
  'Попробуйте ввести другое имя пользователи или подключиться позднее.';

type dataType = UsernameBlockProps & ChatWindowProps;

export default () => {
  const [users, setUser] = useState<dataType['users']>([]);
  const [messages, setMessages] = useState<dataType['messages']>([]);
  const [alreadyUserName, setAlreadyUserName] = useState<boolean>(false);

  const dispatch = useDispatch();
  const username = useSelector(state => state.username);

  const { sendMessage, readyState } = useWebSocket(socketUrl, {
    onMessage: el => handlerReceiveMessage(el),
    onError: el => {
      el.type === 'error' && setAlreadyUserName(true);
    },
    queryParams: {
      username,
    },
  });

  const handlerReceiveMessage = receiveData => {
    const date = new Date();
    const data = JSON.parse(receiveData.data);

    if (data.username) {
      setMessages(prev => {
        const newMessage = {
          user: data.username,
          message: data.message,
          date:
            date.toLocaleDateString('en-GB') +
            ' ' +
            date.toLocaleTimeString('en-GB'),
        };
        const newMessages = [...prev, newMessage];
        return newMessages;
      });
    }

    data.usersList && setUser(data.usersList);
  };

  const handlerSendMessage = (text: string) => {
    const data = {
      message: text,
    };
    sendMessage(JSON.stringify(data));
  };

  const handlerClickModal = () => {
    dispatch(logout());
  };

  useEffect(() => {
    if ((readyState === 3 || readyState === -1) && !alreadyUserName)
      dispatch(logout());
  }, [readyState]);

  return (
    <StyledChat>
      {(readyState === 3 || readyState === -1) && !alreadyUserName ? (
        <Redirect to="/authorization" />
      ) : alreadyUserName ? (
        <ModalMessage
          message={errorModalMessage}
          handlerClick={handlerClickModal}
        />
      ) : readyState === 1 ? (
        <>
          <div className="chat-wrapper">
            <UsernameBlock users={users} />
            <ChatWindow messages={messages} />
            <ChatInput handlerSendMessage={handlerSendMessage} />
          </div>
        </>
      ) : (
        <LoadScreen />
      )}
    </StyledChat>
  );
};
