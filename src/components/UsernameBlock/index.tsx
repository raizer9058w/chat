import React, { FC } from 'react';
import styled from 'styled-components';

const StyledUsernameBlock = styled.div`
  grid-area: 1 / 1 / 3 / 1;
  background-color: var(--wall);
  border: 2px solid white;
  box-shadow: 0px 0px 15px var(--wall);
  overflow: hidden;

  & > div:nth-child(even) {
    background-color: var(--honey-mid);
  }

  & > div:nth-child(odd) {
    background-color: var(--terracot-mid);
  }

  & > div {
    width: calc(100% - 20px);
    height: 35px;
    display: flex;
    align-items: center;
    padding-left: 20px;
  }

  p {
    color: var(--white);
    margin: 0;
    font-weight: 700;
    font-size: 20px;
  }
`;

export interface UsernameBlockProps {
  users: Array<string>;
}

export const UsernameBlock: FC<UsernameBlockProps> = ({ users }) => {
  return (
    <StyledUsernameBlock>
      {users.map(el => (
        <div key={el}>
          <p>{el}</p>
        </div>
      ))}
    </StyledUsernameBlock>
  );
};
