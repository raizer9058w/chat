import React from 'react';
import { useDispatch } from 'react-redux';
import styled from 'styled-components';

import { login } from '../../store/action';

const StyledLoginForm = styled.div`
  position: fixed;
  top: calc((100vh - 15vh) / 2);
  left: calc((100vw - 25vw) / 2);
  animation: animatedForm 1.5s cubic-bezier(0.35, 1.5, 0.65, 1.5);

  @keyframes animatedForm {
    from {
      transform: translateY(-50vh);
      opacity: 0.1;
    }
    to {
      transform: translateY(0);
      opacity: 1;
    }
  }

  form {
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    width: 25vw;
    height: 20vh;
    background-color: var(--wall);
    border: 2px solid var(--white);
    border-radius: 10px;
    box-shadow: 5px 10px 15px var(--black);

    input:first-child {
      width: 50%;
      height: 17%;
      margin: 0 25%;
      border: 2px solid var(--wall);
      text-align: center;
      border-radius: 5px;

      &::placeholder {
        text-align: center;
      }

      &:focus {
        border: 2px solid var(--biscotti);
      }
    }

    input:last-child {
      width: 25%;
      height: 20%;
      border: 1px solid var(--black);
      border-radius: 15px;
      background-color: var(--white);
      color: var(--black);

      &:hover {
        cursor: pointer;
        background-color: var(--biscotti);
        color: var(--white);
      }
    }

    input {
      &:focus {
        outline: none;
      }
    }
  }
`;

export const LoginForm = () => {
  const dispatch = useDispatch();

  const handlerForm = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    if (e.target[0].value.length > 0) {
      dispatch(login(e.target[0].value));
    }
  };

  return (
    <StyledLoginForm>
      <form onSubmit={handlerForm}>
        <input type="text" placeholder="Введи свое имя" />
        <input type="submit" value="Войти" />
      </form>
    </StyledLoginForm>
  );
};
