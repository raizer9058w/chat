import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import styled from 'styled-components';

import { logout } from '../../store/action';

const StyledHeaderBlock = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100vw;
  height: 75px;
  background-color: var(--terracot);

  .wrapper-content {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    max-width: 1920px;
    width: 100%;
    height: 100%;
    margin: auto;

    h1 {
      color: var(--white);
      text-shadow: 0px 0px 2px var(--honey), 0px 0px 3px var(--white);
    }

    div {
      display: flex;
      justify-content: flex-end;
      align-items: center;
      flex-wrap: wrap;
      width: calc(50% - 125px);
      padding: 10px 25px;

      h3 {
        width: 100%;
        margin-block-start: 0;
        margin-block-end: 10px;
        text-align: right;
        color: var(--white);
        text-shadow: 0px 0px 2px var(--honey), 0px 0px 3px var(--white);
      }

      button {
        border: none;
        border-radius: 5px;
        background-color: var(--white);
        color: var(--black);
        padding: 3px 10px;
        transition: all ease 0.5s;
        transform: translateY(0) scale(1);

        &:hover {
          cursor: pointer;
          background-color: var(--glaze);
          color: var(--white);
          transform: translateY(-2px) scale(1.05);
          text-shadow: 1px 1px 3px var(--black);
        }

        &:focus {
          outline: none;
        }
      }
    }
  }
`;

export const HeaderBlock = () => {
  const dispatch = useDispatch();
  const username = useSelector(state => state.username);

  const handlerButton = () => {
    dispatch(logout());
  };

  return (
    <StyledHeaderBlock>
      <div className="wrapper-content">
        <h1>RaZi Chat</h1>
        <div className="user-block">
          <h3>{username}</h3>
          <button onClick={handlerButton}>Выйти</button>
        </div>
      </div>
    </StyledHeaderBlock>
  );
};
