import React, { FC } from 'react';
import styled from 'styled-components';

const StyledModalMessage = styled.div`
  position: fixed;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100vh;

  .background-style {
    position: absolute;
    width: 100%;
    height: 100%;
    background-color: var(--honey);
  }

  .modal-content-block {
    position: absolute;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-wrap: wrap;
    width: 30vw;
    height: 20vh;
    padding: 25px 50px;
    box-sizing: border-box;
    background-color: var(--terracot);
    border: 2px solid var(--white);
    border-radius: 10px;
    box-shadow: 5px 10px 15px var(--black);

    p {
      text-align: center;
      color: white;
      font-size: 20px;
      letter-spacing: 0.5px;
      font-weight: 500;
    }

    button {
      width: 25%;
      height: 20%;
      border: 1px solid var(--black);
      border-radius: 15px;
      background-color: var(--white);
      color: var(--black);

      &:hover {
        cursor: pointer;
        background-color: var(--biscotti);
        color: var(--white);
      }

      &:focus {
        outline: none;
      }
    }
  }
`;

interface ModalMessageProps {
  message: string;
  handlerClick: () => void;
}

export const ModalMessage: FC<ModalMessageProps> = ({
  message,
  handlerClick,
}) => {
  return (
    <StyledModalMessage>
      <div className="background-style" />
      <div className="modal-content-block">
        <p>{message}</p>
        <button onClick={handlerClick}>Ок</button>
      </div>
    </StyledModalMessage>
  );
};
