import React, { FC, useEffect, useRef } from 'react';
import { useSelector } from 'react-redux';
import styled from 'styled-components';
import classNames from 'classnames';

import { IState } from '../../store/reducer';

const systemMessage = 'Системное сообщение';

const StyledChatWindow = styled.div`
  background-color: var(--white);
  border: 1px solid var(--wall);
  box-shadow: 0px 0px 10px var(--wall);
  padding: 10px 30px;
  overflow-y: auto;

  &::-webkit-scrollbar {
    width: 5px;
  }

  &::-webkit-scrollbar-track {
    box-shadow: inset 0 0 5px var(--wall);
    border-radius: 10px;
  }

  &::-webkit-scrollbar-thumb {
    background: var(--honey);
    border-radius: 10px;
  }
`;

const StyledMessage = styled.div`
  position: relative;
  width: max-content;
  max-width: calc(100% - 120px);
  min-width: 50%;
  background-color: var(--honey-mid);
  padding: 10px;
  margin-bottom: 25px;
  border-radius: 10px 10px 10px 0px;

  .message-info {
    margin-bottom: 10px;

    .message-info__username {
      margin: 0;
    }

    .message-info__date {
      margin: 0;
    }
  }

  & > .message-text {
    max-width: 100%;
    margin: 0;
    white-space: pre-line;
  }

  &::before {
    content: '';
    position: absolute;
    bottom: -16px;
    left: 0;
    width: 0;
    height: 0;
    border: 8px solid;
    border-color: var(--honey-mid) transparent transparent var(--honey-mid);
  }

  &.self-message {
    background-color: var(--ocean-mid);
    margin-left: auto;
    border-radius: 10px 10px 0px 10px;
  }

  &.self-message::before {
    content: '';
    position: absolute;
    bottom: -16px;
    left: calc(100% - 16px);
    width: 0;
    height: 0;
    border: 8px solid;
    border-color: var(--ocean-mid) var(--ocean-mid) transparent transparent;
  }

  &.system-message {
    margin-left: auto;
    margin-right: auto;
    text-align: center;
    background-color: var(--terracot-mid);
    color: var(--white);
    border-radius: 10px 10px 10px 10px;
  }

  &.system-message::before {
    content: none;
  }
`;

interface IMessages {
  user: string;
  message: string;
  date: string;
}

export interface ChatWindowProps {
  messages: Array<IMessages>;
}

type MessageProps = IMessages & Pick<IState, 'username'>;

const Message: FC<MessageProps> = ({ user, message, date, username }) => (
  <StyledMessage
    className={classNames({
      'self-message': username === user,
      'system-message': user === systemMessage,
    })}
  >
    <div className="message-info">
      <h3 className="message-info__username">{user}</h3>
      <p className="message-info__date">{date}</p>
    </div>
    <p className="message-text">{message}</p>
  </StyledMessage>
);

export const ChatWindow: FC<ChatWindowProps> = ({ messages }) => {
  const ref = useRef(null);
  const username = useSelector(state => state.username);

  useEffect(() => {
    const childrenChatBlock = ref.current.children;
    childrenChatBlock[childrenChatBlock.length - 1]?.scrollIntoView({
      behavior: 'smooth',
    });
  }, [messages]);

  return (
    <StyledChatWindow ref={ref}>
      {messages?.map((el, index) => {
        return <Message {...el} username={username} key={el.user + index} />;
      })}
    </StyledChatWindow>
  );
};
