import React, { useEffect, useState, useRef, FC } from 'react';
import styled from 'styled-components';
import classNames from 'classnames';
import Picker, { IEmojiData } from 'emoji-picker-react';

const StyledChatInput = styled.form`
  display: flex;
  align-items: center;
  background-color: var(--white);
  border: 1px solid var(--wall);
  box-shadow: 0px 0px 10px var(--wall);

  textarea {
    width: calc(100% - 225px);
    height: 100%;
    box-sizing: border-box;
    border: none;
    outline: none;
    resize: none;
    padding: 10px;
    font-size: 18px;
    font-weight: 500;
    letter-spacing: 0.5px;

    &::-webkit-scrollbar {
      width: 5px;
    }

    &::-webkit-scrollbar-track {
      box-shadow: inset 0 0 5px var(--wall);
      border-radius: 10px;
    }

    &::-webkit-scrollbar-thumb {
      background: var(--terracot-mid);
      border-radius: 10px;
    }
  }

  .emoji-picker-wrapper {
    position: relative;
    width: 75px;
    height: 100%;
    display: flex;
    align-items: center;
    justify-content: center;

    & > button {
      width: 100%;
      height: 35%;
      box-sizing: border-box;
      border: 1px solid var(--black);
      border-radius: 15px;
      background-color: var(--glaze);
      color: var(--black);
      font-size: 15px;
      font-weight: 500;
      transition: all ease 0.5s;
      transform: scale(1);
      color: var(--white);

      &:hover {
        cursor: pointer;
        background-color: var(--ocean-mid);
        transform: scale(1.05);
        text-shadow: 1px 1px 3px var(--black);
      }

      &.active-button {
        cursor: pointer;
        background-color: var(--ocean-mid);
        transform: scale(1.05);
        text-shadow: 1px 1px 3px var(--black);
      }

      &:focus {
        outline: none;
      }
    }
  }

  input {
    width: 100px;
    height: 50%;
    margin: auto;
    box-sizing: border-box;
    border: 1px solid var(--black);
    border-radius: 15px;
    background-color: var(--glaze);
    color: var(--black);
    font-size: 15px;
    font-weight: 500;
    transition: all ease 0.5s;
    transform: scale(1);
    color: var(--white);

    &:hover {
      cursor: pointer;
      background-color: var(--ocean-mid);
      transform: scale(1.05);
      text-shadow: 1px 1px 3px var(--black);
    }

    &.active-button {
      cursor: pointer;
      background-color: var(--ocean-mid);
      transform: scale(1.05);
      text-shadow: 1px 1px 3px var(--black);
    }

    &:focus {
      outline: none;
    }
  }
`;

interface ChatInputProps {
  handlerSendMessage: (el: string) => void;
}

export const ChatInput: FC<ChatInputProps> = ({ handlerSendMessage }) => {
  const refMessage = useRef(null);
  const refChatInput = useRef(null);
  const [inputValue, setInputValue] = useState<string>('');
  const [statusSendMessage, setStatusSendMessage] = useState<boolean>(false);
  const [visibleEmodji, setVisibleEmodji] = useState<boolean>(false);

  const handlerInputValue = (e: React.ChangeEvent<HTMLTextAreaElement>) => {
    setInputValue(e.target.value);
  };

  const sendMessage = () => {
    const lengthInput = inputValue.replace(/\n/g, '').length;
    if (lengthInput) {
      setStatusSendMessage(() => {
        setTimeout(() => setStatusSendMessage(false), 150);
        return true;
      });
      setVisibleEmodji(false);
    } else if (inputValue.length) {
      setInputValue('');
    }
  };

  const handlerSubmitForm = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    sendMessage();
  };

  const handlerClickEnter = (e: KeyboardEvent) => {
    if (e.code === 'Enter' && e.shiftKey) {
      e.preventDefault();
      setInputValue(e => e + '\n');
      return;
    } else if (e.code === 'Enter') {
      e.preventDefault();
      sendMessage();
    }
  };

  const handlerEmojiClick = (
    e: React.MouseEvent<Element, MouseEvent>,
    data: IEmojiData
  ) => {
    setInputValue(prev => prev + data.emoji);
    refMessage.current.focus();
  };

  const handlerClickButton = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    e.preventDefault();
    setVisibleEmodji(prev => !prev);
  };

  useEffect(() => {
    if (statusSendMessage) {
      handlerSendMessage(inputValue);
      setInputValue('');
      refMessage.current.focus();
    }
  }, [statusSendMessage]);

  useEffect(() => {
    window.addEventListener('keydown', handlerClickEnter);

    return () => {
      window.removeEventListener('keydown', handlerClickEnter);
    };
  }, [inputValue]);

  return (
    <StyledChatInput onSubmit={handlerSubmitForm} ref={refChatInput}>
      <textarea
        name="input-text"
        id="input-text"
        value={inputValue}
        onChange={handlerInputValue}
        ref={refMessage}
      />
      <div className="emoji-picker-wrapper">
        {visibleEmodji && (
          <Picker
            onEmojiClick={handlerEmojiClick}
            disableSearchBar
            pickerStyle={{
              top: `${refChatInput.current?.offsetHeight / 8 - 320}px`,
              left: '-250px',
              position: 'absolute',
            }}
          />
        )}
        <button onClick={handlerClickButton}>&#128512;</button>
      </div>
      <input
        type="submit"
        value="Отправить"
        className={classNames({ 'active-button': statusSendMessage })}
      />
    </StyledChatInput>
  );
};
