export const login = (username: string) => {
    return {
      type: 'LOGIN',
      username,
    }
}

export const logout = () => {
  return {
    type: 'LOGOUT',
  }
}
