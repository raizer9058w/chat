
import { Reducer } from 'redux';

export interface IState {
    logged: boolean;
    username: string | undefined;
}

const initialState: IState = {
    logged: false,
    username: undefined,
}

export const userInfo: Reducer<IState> = (store = initialState, action): IState => {
  const {type, ...payload} = action;
  
  switch (type) {
    case 'LOGIN':
      return {...store, ...payload, logged: true}
    case 'LOGOUT': 
      return {...store, logged: false, username: undefined}
    default: 
      return store
  }
}
