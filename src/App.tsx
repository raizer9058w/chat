import React from 'react';
import { useSelector } from 'react-redux';

import Pages from './pages';
import { HeaderBlock } from './components/HeaderBlock';

export default () => {
  const isLogged = useSelector(store => store.logged);
  return (
    <div className="App">
      <header className="App-header">{isLogged && <HeaderBlock />}</header>
      <main>
        <Pages />
      </main>
    </div>
  );
};
